﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Core
{
    /// <summary>
    /// 操作结果 
    /// </summary>
    public class OperationResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Sucess { get; set; }

        /// <summary>
        /// 错误列表
        /// </summary>
        public List<String> Errors { get; set; }

        /// <summary>
        /// 操作结果
        /// </summary>
        public OperationResult()
        {
            Errors = new List<string>();
            Sucess = true;
        }
    }
}
