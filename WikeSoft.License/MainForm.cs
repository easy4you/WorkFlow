﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using WikeSoft.Core;
using WikeSoft.Core.License;
using WikeSoft.Core.Security;

namespace WikeSoft.License
{
    /// <summary>
    /// 主页
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }

      
        private void button1_Click(object sender, EventArgs e)
        {
            LicenseEntity entity = new LicenseEntity();
            entity.Key = this.textBox1.Text;
            entity.ExpireDate = this.dateTimePicker1.Value.Date;
            string json = JsonConvert.SerializeObject(entity);
            this.textBox3.Text = DesEncrypt.Encode(json);

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string mac = Mac.GetMacAddressByWmi();
            string key = DesEncrypt.Encode(mac);
            this.textBox1.Text = key;
        }
    
    }
}
