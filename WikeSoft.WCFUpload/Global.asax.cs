﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Microsoft.ServiceModel.Web;

namespace WikeSoft.WCFUpload
{
    /// <summary>
    /// 全局
    /// </summary>
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// 开始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        /// <summary>
        /// 注册路由
        /// </summary>
        private void RegisterRoutes()
        {

            WebServiceHost2Factory f2 = new WebServiceHost2Factory();  //可以用于异常处理

            RouteTable.Routes.Add(new ServiceRoute("file", f2, typeof(FileService)));
        }
    }
}