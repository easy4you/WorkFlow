﻿using System.ComponentModel;

namespace WikeSoft.Enterprise.Enum
{
    /// <summary>
    /// 用户状态
    /// </summary>
    public enum UserStatus
    {
        /// <summary>
        /// 在职
        /// </summary>
        [Description("在职")]
        Enabled = 1,

        /// <summary>
        /// 离职
        /// </summary>
        [Description("离职")]
        Disabled = 2,

         
    }
 
}
