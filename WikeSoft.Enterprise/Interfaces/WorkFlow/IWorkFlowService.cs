﻿using System;
using System.Collections.Generic;
using WikeSoft.WorkFlowEngine.Models;

namespace WikeSoft.Enterprise.Interfaces.WorkFlow
{
    /// <summary>
    /// 流程服务
    /// </summary>
    public interface IWorkFlowService
    {
        /// <summary>
        /// 用户
        /// </summary>
        /// <param name="authority"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        string GetAuthorityUser(WorkFlowAuthority authority, string userId);

        /// <summary>
        /// 用户列表
        /// </summary>
        /// <param name="authorities"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        string GetAuthorityUsers(List<WorkFlowAuthority> authorities, string userId );

       

    }
}
