﻿using System;
using System.Collections.Generic;
using WikeSoft.Core;
using WikeSoft.Data.Models.Sys;
using WikeSoft.Enterprise.Models.Filters.Sys;

namespace WikeSoft.Enterprise.Interfaces.Sys
{
    /// <summary>
    /// 
    /// </summary>
    public interface IKeyValueService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(KeyValueAddModel model);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Edit(KeyValueEditModel model);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        KeyValueEditModel Find(Guid id);

        /// <summary>
        /// 分页查询信息
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        PagedResult<KeyValueModel> Query(KeyValueFilter filter);
        /// <summary>
        /// 查询全部
        /// </summary>
        /// <returns></returns>
        List<KeyValueModel> GetAll();

        /// <summary>
        /// 得到值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string GetValue(string key);

    }
}
