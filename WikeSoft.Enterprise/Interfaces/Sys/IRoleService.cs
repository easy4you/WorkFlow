﻿using System;
using System.Collections.Generic;
using WikeSoft.Core;
using WikeSoft.Data.Models;
using WikeSoft.Data.Models.Sys;
using WikeSoft.Enterprise.Enum;
using WikeSoft.Enterprise.Models;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Enterprise.Interfaces.Sys
{
    /// <summary>
    /// 角色
    /// </summary>
    public interface IRoleService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(RoleAddModel model);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Edit(RoleEditModel model);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        RoleEditModel Find(string id);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids">id集合</param>
        /// <returns></returns>
        bool Delete(IList<string> ids);

        /// <summary>
        /// 分页查询信息
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        PagedResult<RoleModel> Query(RoleFilter filter);

        /// <summary>
        /// 验证角色名是否重复
        /// </summary>
        /// <param name="roleId">roleId，可以为空，为空表示添加</param>
        /// <param name="roleName">角色名称</param>
        /// <returns></returns>
        bool IsExists(string roleId, string roleName);

        /// <summary>
        /// 设置指定的角色能够访问的页面
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <param name="pageIds">页面ID集合</param>
        /// <returns></returns>
        bool SetRights(string roleId, IList<string> pageIds);

     

        /// <summary>
        /// 获取所有的角色
        /// </summary>
        /// <param name="id">用户/页面ID</param>
        /// <param name="type">角色类型</param>
        /// <returns></returns>
        List<RoleModel> GetAllRoles(string id, RoleType type);

        /// <summary>
        /// 获取所有的ztree模型树
        /// </summary>
        /// <returns></returns>
        List<ZTreeModel> GetRoleTrees();

        /// <summary>
        /// 根据角色ID获取角色的权限
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <returns></returns>
        List<string> GetRoleRights(string roleId);
    }
}
