﻿using System;
using System.Collections.Generic;
using WikeSoft.Core;
using WikeSoft.Enterprise.Models;
using WikeSoft.Enterprise.Models.Filters;


namespace WikeSoft.Enterprise.Interfaces
{
    /// <summary>
    /// 请假
    /// </summary>
    public interface IHolidayApplyService
    {
       
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        string Add(HolidayModel data);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="data"></param>
        void Edit(HolidayModel data);

        /// <summary>
        /// 得到
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        HolidayModel Get(string id);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        void Delete(IList<string> ids);

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        PagedResult<HolidayModel> GetList(HolidayFilter filter);
 
        /// <summary>
        /// 开始流程
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool StartWorkFlow(IList<string> ids, string userId);
       
        /// <summary>
        /// 驳回流程
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        bool RejectWorkFlow(string ids);
        
        /// <summary>
        /// 完成流程
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool FinishWorkFlow(string id);
    }
}
