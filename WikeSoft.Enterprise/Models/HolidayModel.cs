﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WikeSoft.Enterprise.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class HolidayModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)

        ///<summary>
        /// 请假人
        ///</summary>

        [Display(Name = "申请人")]
        public string Person { get; set; } // Person (length: 50)

        /// <summary>
        /// 请假人
        /// </summary>

      
        public string UserId { get; set; }
        ///<summary>
        /// 开始时间
        ///</summary>
        [Display(Name = "开始时间")]
        [Required(ErrorMessage = "开始时间必填")]
        public System.DateTime? StartDate { get; set; } // StartDate

        ///<summary>
        /// 结束时间
        ///</summary>
        [Display(Name = "结束时间")]
        [Required(ErrorMessage = "结束时间必填")]
        public System.DateTime? EndDate { get; set; } // EndDate

        ///<summary>
        /// 请假天数
        ///</summary>
        [Display(Name = "请假天数")]
        [Required(ErrorMessage = "请假天数必填")]
        public decimal? Days { get; set; } // Days

        ///<summary>
        /// 事由
        ///</summary>
        [Display(Name = "事由")]
        [Required(ErrorMessage = "事由必填")]
        public string Remark { get; set; } // Remark (length: 100)

        ///<summary>
        /// 流程Id
        ///</summary>
        public string FlowId { get; set; } // FlowId (length: 50)
        /// <summary>
        /// 
        /// </summary>

        public string TaskName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreateDate { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class HolidayAuditModel
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// InstanceId
        /// </summary>
        public string InstanceId { get; set; }

        /// <summary>
        /// TaskId
        /// </summary>
        public string TaskId { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        [Display(Name = "审批意见")]
        public string Message { get; set; }

        /// <summary>
        /// Agree
        /// </summary>
        public bool Agree { get; set; }
    }
}
