﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WikeSoft.Enterprise.Models.Sys
{
    /// <summary>
    /// 导航树模型
    /// </summary>
    public class NavTreeModel
    {
        /// <summary>
        /// 导航树模型
        /// </summary>
        public NavTreeModel()
        {
            Children = new List<NavTreeModel>();
        }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// 父节点ID
        /// </summary>
        [JsonIgnore]
        public string ParentId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// 是否展开
        /// </summary>
        [JsonProperty("spread")]
        public bool Spread { get; set; }

        /// <summary>
        /// 目录
        /// </summary>
        [JsonProperty("alias")]
        public string Alias { get; set; }

        /// <summary>
        /// 连接地址
        /// </summary>
        [JsonProperty("href")]
        public string Href { get; set; }

        /// <summary>
        /// Url
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }

        /// <summary>
        /// 子节点
        /// </summary>
        [JsonProperty("children")]
        public List<NavTreeModel> Children { get; set; }
    }
}
