﻿using System.Collections.Generic;

namespace WikeSoft.Enterprise.Models.Filters
{
    /// <summary>
    /// 过滤
    /// </summary>
    public class HolidayFilter: BaseFilter
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 流程ID
        /// </summary>
        public List<string> FlowIds { get; set; }
    }
}
