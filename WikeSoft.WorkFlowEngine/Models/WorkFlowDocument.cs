﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    public class WorkFlowDocument
    {
        


        public List<TaskDefinition> Nodes { get; set; }

        public List<TaskLink> NodeLinks { get; set; }
    }
}
