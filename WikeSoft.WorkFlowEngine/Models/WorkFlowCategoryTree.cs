﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    /// <summary>
    /// 导航树模型
    /// </summary>
    public class WorkFlowCategoryTree
    {
        public WorkFlowCategoryTree()
        {
            Children = new List<WorkFlowCategoryTree>();
        }

        /// <summary>
        /// Id
        /// </summary>
         
        public string Id { get; set; }

        /// <summary>
        /// 父节点ID
        /// </summary>
        
        public string ParentId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        
        public string Name { get; set; }

        /// <summary>
        /// 是否展开
        /// </summary>
         
        public bool Spread { get; set; }

        /// <summary>
        /// 目录
        /// </summary>
         
        public string Alias { get; set; }

        /// <summary>
        /// 连接地址
        /// </summary>
         
        public string Href { get; set; }

        
        public string Url { get; set; }

        /// <summary>
        /// 子节点
        /// </summary>
        
        public List<WorkFlowCategoryTree> Children { get; set; }
    }
}
