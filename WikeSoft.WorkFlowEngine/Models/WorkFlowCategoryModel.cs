﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{ 

    public class WorkFlowCategoryModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key) (length: 50)

        ///<summary>
        /// 流程类别名称
        ///</summary>
        [Display(Name = "流程类别名称")]
        public string CategoryName { get; set; } // CategoryName (length: 500)

        ///<summary>
        /// 排序
        ///</summary>
        [Display(Name = "排序")]
        public int? SortIndx { get; set; } // SortIndx

        ///<summary>
        /// 上级节点
        ///</summary>
        public string ParentId { get; set; } // ParentId (length: 50)

        [Display(Name = "上级节点")]
        public string ParentName { get; set; }
    }
}
