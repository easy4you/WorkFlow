﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Msg
{
    public class InstanceMessage:FlowMessage
    {

        

        /// <summary>
        /// 流程Id
        /// </summary>
        public string FlowId { get; set; }

        /// <summary>
        /// 节点Id
        /// </summary>
        public string NodeId { get; set; }
        
        /// <summary>
        /// 流程是否完成
        /// </summary>
        public bool Completed { get; set; }

        public InstanceMessage():base()
        {
            Completed = false;
        }
    }
}
