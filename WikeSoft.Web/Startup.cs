﻿using Microsoft.Owin;
using Owin;
using WikeSoft.Web;

[assembly: OwinStartup(typeof(Startup))]
namespace WikeSoft.Web
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Startup
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
