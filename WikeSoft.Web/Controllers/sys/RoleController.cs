﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

using WikeSoft.Data.Models.Sys;
using WikeSoft.Enterprise.Interfaces.Sys;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Web.Controllers.Sys
{
    /// <summary>
    /// Role
    /// </summary>
    public class RoleController : BaseController
    {
        private readonly IRoleService _roleService;
      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleService"></param>
        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        // GET: Role
       
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(RoleAddModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _roleService.Add(model);
                if (success)
                    return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {
            var model = _roleService.Find(id);
            return View(model);
        }

        /// <summary>
        /// 设置角色权限
        /// </summary>
        /// <returns></returns>
        public ActionResult SetRights()
        {
            return View();
        }

        /// <summary>
        /// 根据角色ID获取角色的权限
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns></returns>
        public JsonResult GetPageTreesByRoleId(string id)
        {
            var rights = _roleService.GetRoleRights(id);
            return JsonOk(rights);
        }

        /// <summary>
        /// 设置角色权限
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SetRoleRights(SetRoleRightModel model)
        {
            var success = _roleService.SetRights(model.RoleId, model.PageIds);
            if (success)
            {
                return Json(true);
            }
            else
            {
                return Json(false, "页面权限未发生任何变化");
            }

          
           
        }

        /// <summary>
        /// 清空角色权限
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult RemoveRoleRights(string id)
        {
            var success = _roleService.SetRights(id, null);
            return Json(success);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(RoleEditModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _roleService.Edit(model);
                if (success)
                    return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(IList<string> ids)
        {
            var success = _roleService.Delete(ids);
            return Json(success);
        }

        /// <summary>
        /// 判断角色名是否存在
        /// </summary>
        /// <returns></returns>
        public ActionResult IsExists(string Id, string RoleName)
        {
            var exists = _roleService.IsExists(Id, RoleName);
            return JsonOk(!exists);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetList(RoleFilter filter)
        {
            var result = _roleService.Query(filter);
            return JsonOk(result);
        }

        /// <summary>
        /// 获取所有ztree结构的角色数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetRoles()
        {
            var pages = _roleService.GetRoleTrees();
            return JsonOk(pages);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectRole()
        {
            return View();
        }
    }
}