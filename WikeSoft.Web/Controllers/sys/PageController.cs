﻿using System;
using System.Web.Mvc;
using WikeSoft.Core.Extension;


using WikeSoft.Data.Models.Filters;
using WikeSoft.Enterprise.Enum;
using WikeSoft.Enterprise.Interfaces.Sys;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Web.Controllers.Sys
{
    /// <summary>
    /// 页面管理
    /// </summary>
    public class PageController : BaseController
    {
        private readonly IPageService _pageService;
        private readonly IRoleService _roleService;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="pageService"></param>
        /// <param name="roleService"></param>
        public PageController(IPageService pageService,IRoleService roleService)
        {
            _pageService = pageService;
            _roleService = roleService;
        }

        // GET: Page
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="id">父页面ID</param>
        /// <returns></returns>
        
        public ActionResult Add(string id)
        {
            PageEditModel parent = null;
            if (id.IsNotBlank())
            {
                parent = _pageService.Find(id);
            }
            var model = new PageAddModel();
            if (parent != null)
            {
                model.ParentId = parent.Id;
                model.ParentPageName = parent.PageName;
            }
            model.IsUsed = true;//默认为启用
            return View(model);
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(PageAddModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _pageService.Add(model);
                if (success)
                {
                    return PartialView("CloseLayerPartial");
                }
            }
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
       
        public ActionResult Edit(string id)
        {
            var model = _pageService.Find(id);
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(PageEditModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _pageService.Edit(model);
                if (success)
                {
                    return PartialView("CloseLayerPartial");
                }
                return PartialView("CloseLayerPartial");
            }
            return View(model);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">主键ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(string id)
        {
            var success = _pageService.Delete(id);
            return Ok(success);
        }

        /// <summary>
        /// 根据父节点Id获取树
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetTrees(ZTreeFilter model)
        {
            var parentId = model.id.IsBlank() ? string.Empty : model.id;
            var pages = _pageService.GetByParentId(parentId, null);
            return JsonOk(pages);
        }

        /// <summary>
        /// 获取所有ztree结构的页面数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPageTrees()
        {
            var pages = _pageService.GetTrees();
            return JsonOk(pages);
        }

        /// <summary>
        /// 页面角色
        /// </summary>
        /// <param name="id">页面ID</param>
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult RolePartial(string id)
        {
            var roles = _roleService.GetAllRoles(id, RoleType.Page);
            return PartialView("RolePartial", roles);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ListView()
        {
            return View();
        }


        /// <summary>
        /// 岗位查询
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Query(PageFilter filter)
        {
            var result = _pageService.Query(filter);
            return JsonOk(result);
        }
    }
}