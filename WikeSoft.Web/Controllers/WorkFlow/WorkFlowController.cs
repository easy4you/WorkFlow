﻿using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;
using WikeSoft.WorkFlowEngine.Interfaces;

namespace WikeSoft.Web.Controllers.WorkFlow
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkFlowController : BaseController
    {
        private readonly IWorkFlowInstanceService _flowInstanceService;
      
        /// <summary>
        /// 
        /// </summary>
        public WorkFlowController()
        {
            _flowInstanceService = DependencyResolver.Current.GetService<IWorkFlowInstanceService>();
        }
        // GET: WorkFlow
        /// <summary>
        /// 
        /// </summary>
        /// <param name="flowId"></param>
        public void Pic(string flowId)
        {
            var image = _flowInstanceService.GetRunBitmap(flowId);
            MemoryStream stream = new MemoryStream();
            image.Save(stream, ImageFormat.Jpeg);
            HttpContext.Response.Clear();
            HttpContext.Response.ContentType = "image/jpeg";
            HttpContext.Response.BinaryWrite(stream.ToArray());

        }
    }
}