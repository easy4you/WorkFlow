﻿/*
** 此js用作主框架页面/home/index初始化用
*/
(function () {
    layui.use(['tree', 'layer'], function() {
        var layer = layui.layer, $ = layui.jquery;
        $.get("/Home/GetTrees", function(datas) {
            if (datas) {
                layui.tree({
                    elem: '#navTree',
                    target: '_blank',
                    click: function (item) { //点击节点回调
                        //console.log(item);
                        if (item.url !== null && item.url !== "" && item.url !== "#") {
                            $("#ifmContent").attr("src", item.url);
                        }
                        return false;
                    },
                    nodes: JSON.parse(datas)
                });
            }
        });
    });
})();