﻿/*
** 此js用作框架列表页面初始化及调用
*/
(function () {
    //初始化基础按钮
    $("#btnAdd").click(function () {
        $(this).button("loading");
        window.location.href = $(this).data("url");
    });
    $("#btnEdit").click(function() {
        var row = WikeGrid.GetData();
        if (row != null) {
            $(this).button("loading");
            var url = $(this).data("url");
            if (url.indexOf("?") > 0) {
                url = url + "&id="+row.Id;
            }
            else {
                url = url + "?id="+row.Id;
            }
            window.location.href = url;
        } else {
            top.layer.alert("请选择要编辑的数据");
        }
    });
    $("#btnDelete").click(function() {
        var delDatas = WikeGrid.GetDataTableDeleteData();
        if (delDatas.Len > 0) {
            var btn = $("#btnDelete");
            top.layer.confirm("确认要删除这" + delDatas.Len + "条数据？", {
                btn: ['确认', '取消'] //按钮
            }, function () {
                var url = btn.data("url");
                btn.button('loading');
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify({ ids: delDatas.Data }),
                    contentType: "application/json, charset=utf-8",
                    success: function (data) {
                        btn.button('reset');
                        if (data.success) {
                            top.layer.alert("删除成功");
                            $("#table_list").trigger("reloadGrid");
                        } else {
                            top.layer.alert("删除失败：" + data.message);
                        }
                    }
                });
            }, function () {
                btn.button('reset');
            });
        } else {
            top.layer.alert("请选择要删除的数据！");
        }
    });
    $("#btnSearch").click(function() {
        var postJson = $("#searchToolBar").toJson();
        WikePage.Search(postJson);
        return false;
    });
})();
var WikePage = {
    GoTo: function (btn, url) {
        $(btn).button("loading");
        window.location.href = url;
    },

    //搜索jqGrid
    Search: function (json) {
        var postData = $("#table_list").jqGrid("getGridParam", "postData");
        $.extend(postData, json); 
        $("#table_list").setGridParam({ search: true }).trigger("reloadGrid", [{ page: 1 }]);
    },

    ReLoadCurrentPage: function() {
        var json = $("#searchToolBar").toJson();
        var postData = $("#table_list").jqGrid("getGridParam", "postData");
        $.extend(postData, json);
        var pageIndex = $("#table_list").jqGrid('getGridParam', 'page');
        $("#table_list").setGridParam({ search: true }).trigger("reloadGrid", [{ page: pageIndex }]);
    },

    DoPost: function (btn, url, data, sucCallback, failCallback) {
        var hasBtn = btn != null;
        if (hasBtn) {
            $(btn).button("loading");
        }
        var myRequestAjax = $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "JSON",
            success: function (res) {
                if (hasBtn) {
                    $(btn).button("reset");
                }
                if (res.success) {
                    if (sucCallback == null || typeof (sucCallback) == 'undefined')
                        top.layer.alert("操作成功");
                    else
                        sucCallback.call(this, res);
                } else {
                    if (failCallback == null || typeof (failCallback) == 'undefined')
                        top.layer.alert("操作失败：" + res.message);
                    else
                        failCallback.call(this, res);
                }
            },
            complete: function (xmlHttpRequest, status) { //请求完成后最终执行参数
                if (hasBtn) {
                    $(btn).button("reset");
                }
                if (status === "timeout") {//超时,status还有success,error等值的情况
                    myRequestAjax.abort();
                    top.layer.alert("请求超时，请刷新页面重试");
                }
                if (status === "error") {
                    myRequestAjax.abort();
                    top.layer.alert("请求失败，请刷新页面重试");
                }
            }
        });
    }
}