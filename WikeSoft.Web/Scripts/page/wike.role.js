﻿var roleSetting = {
    async: {
        enable: true,
        url: "/Role/GetRoles",
        autoParam: ["id", "name=n", "level=lv"],
        otherParam: { "otherParam": "tree" }
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        onClick: onClick
    }
};
var menuSetting = {
    async: {
        enable: true,
        url: "/Page/GetPageTrees",
        autoParam: ["id", "name=n", "level=lv"],
        otherParam: { "otherParam": "tree" }
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    check: {
        enable: true,
        chkboxType: { "Y": "ps", "N": "ps" }
    }
};

function onClick(e, treeId, treeNode) {
    $("#txtRoleId").val(treeNode.id);
    var zTree = $.fn.zTree.getZTreeObj("menuTree");
    zTree.checkAllNodes(false);
    $.ajax({
        url: "/Role/GetPageTreesByRoleId/" + treeNode.id,
        type: "get",
        dataType: "json",
        success: function (res) {
            for (var i = 0, id; id = res[i]; i++) {
                var node = zTree.getNodeByParam("id", id);
                zTree.checkNode(node, true, false, false);
            }
        }
    });
}

function saveData() {
    var roleId = $("#txtRoleId").val();
    if (typeof (roleId) != "undefined" && roleId !== "") {
        var menuTree = $.fn.zTree.getZTreeObj("menuTree");
        var menus = menuTree.getCheckedNodes(true);
        if (menus == null || menus.length === 0) {
            parent.layer.alert("请选择要授权的菜单");
        } else {
            var datas = {
                RoleId: roleId,
                PageIds: []
            };
            for (var i = 0, menu; menu = menus[i]; i++) {
                datas.PageIds.push(menu.id);
            }
            var postData = JSON.stringify(datas);
            var btn = $(this);
            btn.button("loading");
            $.ajax({
                url: "/Role/SetRoleRights",
                type: "POST",
                dataType: "json",
                data: postData,
                contentType: "application/json, charset=utf-8",
                success: function (res) {
                    btn.button("reset");
                    if (res.success) {
                        parent.layer.alert("授权成功");
                    } else {
                        parent.layer.alert("授权失败：" + res.message);
                    }
                }
            });
        }
    } else {
        parent.layer.alert("请选择一个角色");
    }
}

function clearData() {
    var roleId = $("#txtRoleId").val();
    if (typeof (roleId)!= "undefined" && roleId !== "") {
        var roleTree = $.fn.zTree.getZTreeObj("roleTree");
        var role = roleTree.getNodeByParam("id", roleId, null);
        var btn = $(this);
        btn.button("loading");
        parent.layer.confirm("您确定要清空【" + role.name + "】下的所有权限?", {
            btn: ['确认', '取消'] //按钮
        }, function () {
            var menuTree = $.fn.zTree.getZTreeObj("menuTree");
            $.ajax({
                url: "/Role/RemoveRoleRights/" + roleId,
                type: "POST",
                dataType: "json",
                data: null,
                success: function (res) {
                    btn.button("reset");
                    menuTree.checkAllNodes(false);
                    if (res.success) {
                        parent.layer.alert("清空成功");
                    } else {
                        parent.layer.alert("清空失败：" + res.message);
                    }
                }
            });
        }, function () {
            btn.button("reset");
        });
    } else {
        parent.layer.alert("请选择一个角色");
    }
}

$(document).ready(function () {
    $.fn.zTree.init($("#roleTree"), roleSetting);
    $.fn.zTree.init($("#menuTree"), menuSetting);
    $("#btnRightSave").unbind("click").click(saveData);
    $("#btnRightClear").unbind("click").click(clearData);
});